drop table if exists student_assessment;
drop table if exists assessments;
drop table if exists student_vle;
drop table if exists vle;
drop table if exists student_registration;
drop table if exists student_info;
drop table if exists courses;

create table courses(
 code_module varchar(45),
 code_presentation varchar(45),
 length int,
 PRIMARY KEY(code_module, code_presentation)
 );
 create table student_info(
 code_module varchar(45), 
 code_presentation varchar(45), 
 id_student int, 
 gender varchar(45), 
 region varchar(45),
 highest_education varchar(45), 
 imd_band varchar(45), 
 age_band varchar(45), 
 num_of_prev_attempts int,
 studied_credits int, 
 disability varchar(45), 
 final_result varchar(45),
 PRIMARY KEY (code_module, code_presentation, id_student),
 FOREIGN KEY (code_module, code_presentation) REFERENCES courses(code_module, code_presentation)
 );
 create table student_registration(
 code_module varchar(45), 
 code_presentation varchar(45), 
 id_student int, 
 date_registration varchar(5),
 date_unregistration varchar(5),
 PRIMARY KEY (code_module, code_presentation, id_student),
 FOREIGN KEY (code_module, code_presentation) REFERENCES courses(code_module, code_presentation),
 FOREIGN KEY (code_module, code_presentation, id_student) REFERENCES student_info(code_module, code_presentation, id_student)
);

create table vle(
 id_site int, 
 code_module varchar(45), 
 code_presentation varchar(45), 
 activity_type varchar(45),
 week_from varchar(5),
 week_to varchar(5),
 PRIMARY KEY (id_site),
 FOREIGN KEY (code_module, code_presentation) REFERENCES courses(code_module, code_presentation)
);
create table student_vle(
 code_module varchar(45), 
 code_presentation varchar(45), 
 id_student int, 
 id_site int, 
 date varchar(5),
 sum_click int,
 FOREIGN KEY (code_module, code_presentation, id_student) REFERENCES student_info(code_module, code_presentation, id_student),
 FOREIGN KEY (id_site) REFERENCES vle(id_site)
 );
 create table assessments(
 code_module varchar(45), 
 code_presentation varchar(45), 
 id_assessment int, 
 assessment_type varchar(45),
 date varchar(5), 
 weight int,
 PRIMARY KEY(id_assessment),
 FOREIGN KEY (code_module, code_presentation) REFERENCES courses(code_module, code_presentation)
);

CREATE INDEX idx_id_student ON student_info(id_student);
create table student_assessment(
 id_assessment int, 
 id_student int, 
 date_submitted int, 
 is_banked int, 
 score float,
 FOREIGN KEY (id_student) REFERENCES student_info(id_student),
 FOREIGN KEY (id_assessment) REFERENCES assessments(id_assessment)
);


LOAD DATA INFILE 'courses.csv' INTO TABLE courses
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

LOAD DATA INFILE 'studentInfo.csv' INTO TABLE student_info
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

LOAD DATA INFILE 'studentRegistration.csv' INTO TABLE student_registration
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

LOAD DATA INFILE 'vle.csv' INTO TABLE vle
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

LOAD DATA INFILE 'studentVle.csv' INTO TABLE student_vle
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

LOAD DATA INFILE 'assessments.csv' INTO TABLE assessments
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

LOAD DATA INFILE 'studentAssessment.csv' INTO TABLE student_assessment
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 LINES;

